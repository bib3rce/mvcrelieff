from sklearn.preprocessing import Imputer
import numpy as np
import pandas as pd
from sklearn.metrics import adjusted_rand_score as ari
from sklearn.metrics import silhouette_score as sc
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import Birch
from sklearn.cluster import SpectralClustering
from sklearn.metrics import calinski_harabasz_score as ch
from sklearn.metrics import davies_bouldin_score as db
from sklearn.metrics import silhouette_samples as smps
import scipy.stats as ss
import os
from norm_silhouette import get_cluster_indices, get_norm_silhouette_SM, get_norm_silhouette_SN 
import math

def prep(x):
    i = Imputer()
    x = i.fit_transform(np.array(x))
    return StandardScaler().fit_transform(x)

def evaluate_clustering(data, clustering):
    sil_score = sc(data, clustering)
    sil_samples = smps(data,clustering)

    indices = get_cluster_indices(clustering)
    norm_sil_score = get_norm_silhouette_SN(indices, sil_score, sil_samples)
    return norm_sil_score


def get_best_Birch(data):
    # Choose the best clustering using the DBSCAN method
    branching_factor=[40,50,60,70,80,90,100]
    n_clusters=range(2,11)
    threshold=[0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

    cluster_labels = []
    cluster_quality = []

    for bf in branching_factor:
        for nc in n_clusters:
            for t in threshold:
                clls = Birch(branching_factor=bf, n_clusters=nc, threshold=t, compute_labels=True).fit_predict(data)
                cluster_labels.append(clls)
                n_scs = evaluate_clustering(data, clls)
                cluster_quality.append(n_scs)

    best_idx = cluster_quality.index(max(cluster_quality))
    return [cluster_labels[best_idx], cluster_quality[best_idx],cluster_quality[best_idx]]

def get_best_KMeans(data):
    # choose the best KMeans clustering based on the number of clusters, way of initialization,
    # and the KMeans algorihm to use
    n_clusters = range(2, min(10, data.shape[0]))
    inits = ['k-means++', 'random']
    algorithms = ['full', 'elkan']

    cluster_quality = []
    cluster_labels = []
    for cl in n_clusters:
        for init in inits:
            for alg in algorithms:
                clls = KMeans(n_clusters=cl, init=init, algorithm=alg).fit_predict(data)
                cluster_labels.append(clls)
                n_scs = evaluate_clustering(data, clls)
                cluster_quality.append(n_scs)

    best_idx = cluster_quality.index(max(cluster_quality))
    # print('KMeans clustering shape: {}'.format(cluster_labels[best_idx].shape))

    return [cluster_labels[best_idx], cluster_quality[best_idx],cluster_quality[best_idx]]

def get_best_AgglomerativeClustering(data):
    # select the best clustering based on the AgglomerativeClustering approach
    n_clusters = range(2,min(10, data.shape[0]))
    affinity = ['euclidean', 'l1', 'l2', 'manhattan']
    linkage = ['complete', 'average', 'single']

    
    cluster_quality = []
    cluster_labels = []
    for nc in n_clusters:
        for a in affinity:
            for l in linkage:
                AgglomerativeClustering(n_clusters=nc, affinity=a, linkage=l).fit_predict(data)

        #     ward can only work with eucledian affinity
        clls = AgglomerativeClustering(n_clusters=nc, affinity='euclidean', linkage='ward').fit_predict(data)
        cluster_labels.append(clls)
        n_scs = evaluate_clustering(data, clls)
        cluster_quality.append(n_scs)

    best_idx = cluster_quality.index(max(cluster_quality))
    # print('AgglomerativeClustering clustering shape: {}'.format(cluster_labels[best_idx].shape))

    return [cluster_labels[best_idx], cluster_quality[best_idx], cluster_quality[best_idx]]

def get_best_SpectralClustering(data):
    n_clusters = range(2,11)

    cluster_quality = []
    cluster_labels = []
    for nc in n_clusters:
        try:
            clls = SpectralClustering(n_clusters=nc, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30).fit_predict(data)
            cluster_labels.append(clls)
            n_scs = evaluate_clustering(data, clls)
            cluster_quality.append(n_scs)
        except:
            clls = [None]*data.shape[1]
            cluster_labels.append(clls)
            cluster_quality.append(-math.inf)

    best_idx = cluster_quality.index(max(cluster_quality))

    # print('Spectral clustering shape: {}'.format(cluster_labels[best_idx].shape))

    return [cluster_labels[best_idx], cluster_quality[best_idx], cluster_quality[best_idx]]


def get_best_cluster(data):
    cluster_names = ['KMeans',
                    'AgglomerativeClustering',
                    'Birch',
                    'SpectralClustering',
                     ]

    best_clusters = [get_best_KMeans(data),
                     get_best_AgglomerativeClustering(data),
                     get_best_Birch(data),
                     get_best_SpectralClustering(data),
                     ]

    cluster_quality = [cluster[1] for cluster in best_clusters]
    best_idx = cluster_quality.index(max(cluster_quality))

    # print('Best clusters shape {}'.format(best_clusters[best_idx][0].shape))

    return cluster_names[best_idx], best_clusters[best_idx][0], cluster_quality[best_idx], best_clusters[best_idx][2]