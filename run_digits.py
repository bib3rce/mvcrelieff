from __future__ import division
from sklearn.cluster import MeanShift, estimate_bandwidth
from collections import Counter
from sklearn.preprocessing import Imputer
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn import metrics
import sys
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import spectral_clustering
from sklearn.cluster import AffinityPropagation
from sklearn import cluster
import os
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Imputer
from get_best_clusters_norm2 import get_optimal_clusters as get_best_cluster
from get_best_clusters_norm2 import evaluate_clustering
from sklearn.metrics import adjusted_rand_score as ari
import Cluster_Ensembles as CE
from sklearn.metrics import normalized_mutual_info_score as nmi

import sys
import warnings

from datetime import datetime

if not sys.warnoptions:
    warnings.simplefilter("ignore")

# try:
import mvFeatureSelector_MV as mvFeatureSelector
import mvFeatureSelector_TM
# except ImportError:
#     print 'importing modulename failed'
#     sys.exit()

def prep(x):
    i = Imputer()
    x = i.fit_transform(np.array(x))
    return StandardScaler().fit_transform(x)

mvReliefF_version = 2

address = r'./data_digits'
    
d1 = pd.read_csv('data_digits/mfeat-fac2.txt',sep=' ', header=None)
d2 = pd.read_csv('data_digits/mfeat-fou',sep=' ', header=None)

d1_columns = d1.columns.values.tolist()
d2_columns = d2.columns.values.tolist()

# there are 10 classes going from 1 to 10 in ascending order
y = [i//200 for i in range(2000)]

spec = cluster.SpectralClustering(n_clusters=10, eigen_solver='arpack', affinity="nearest_neighbors")



start_time = datetime.now().time().strftime("%H:%M:%S")
print('Start Time = {}'.format(start_time))


mff = mvFeatureSelector.MultilayerFeature([d1,d2], mvReliefF_version, [spec, spec], [d1_columns, d2_columns], metrics.adjusted_rand_score, y, False)
mff_results = mff.fit()


clusters, features, aris = mff.selectBestSubset(address)
drop_features1 = set(d1.columns.values.tolist()).difference(set(features[0]))
drop_features2 = set(d2.columns.values.tolist()).difference(set(features[1]))

print('Drop {} features from View1 and {} features from View 2'.format(len(drop_features1), len(drop_features2)))


d1_MV_post = d1.drop(drop_features1, axis=1)
d2_MV_post = d2.drop(drop_features2, axis=1)

print('View 1 has shape {} after postprocessing. View 2 has shape {} after postprocessing.'.format(d1_MV_post.shape, d2_MV_post.shape))

print(features[0])
print(d1_MV_post.columns.values.tolist())
print(features[1])
print(d2_MV_post.columns.values.tolist())


drop_features1 = set(d1.columns.values.tolist()).difference(set(features[0]))
drop_features2 = set(d2.columns.values.tolist()).difference(set(features[1]))

print('Drop {} features from View1 and {} features from View 2'.format(len(drop_features1), len(drop_features2)))


d1_MV_post = d1.drop(drop_features1, axis=1)
d2_MV_post = d2.drop(drop_features2, axis=1)

data = [prep(d1_MV_post), prep(d2_MV_post)]

# clusters = []
for view in range(len(features)):

	best_cluster = clusters[view]
	print('View {} has {} features and {} clusters. The quality of clustering is {}.'.format(view, data[view].shape[1], len(set(best_cluster)), round(evaluate_clustering(data[view], best_cluster), 5)))

	print('View {} has alignmenet with ground truth of {}'.format(view, ari(best_cluster, y)))
	print('View {} has alignmenet with ground truth of {} using NMI.'.format(view, nmi(best_cluster, y)))


print('The alignment between View 1 and View 2 is {}.'.format(ari(clusters[0], clusters[1])))
print('The NMI alignment between View 1 and View 2 is {}.'.format(nmi(clusters[0], clusters[1])))

ens_clusters_AV = np.array(clusters)
max_cl = max([len(set(clusters[i])) for i in range(len(clusters))])

consensus_clustering_labels = CE.cluster_ensembles(ens_clusters_AV, verbose = True, N_clusters_max = max_cl)

print('The alignment between the consensus clustering labels and the ground truth is {}.'.format(ari(consensus_clustering_labels, y)))
print('The NMI alignment between the consensus clustering labels and the ground truth is {}.'.format(nmi(consensus_clustering_labels, y)))


end_time = datetime.now().time().strftime("%H:%M:%S")
print('End Time = {}'.format(end_time))

total_time=(datetime.strptime(end_time,'%H:%M:%S') - datetime.strptime(start_time,'%H:%M:%S'))
print('Processing time for proposed MV approach: {}'.format(total_time))


print('\n{}\n\n{}\n\n'.format('*'*200,'*'*200))
