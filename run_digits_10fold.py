from __future__ import division
from sklearn.cluster import MeanShift, estimate_bandwidth
from collections import Counter
from sklearn.preprocessing import Imputer
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn import metrics
import sys
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import spectral_clustering
from sklearn.cluster import AffinityPropagation
from sklearn import cluster
import os
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Imputer
from get_best_clusters_norm import get_best_cluster, evaluate_clustering
from sklearn.metrics import adjusted_rand_score as ari
import Cluster_Ensembles as CE
from sklearn.metrics import normalized_mutual_info_score as nmi

import sys
import warnings

from datetime import datetime

if not sys.warnoptions:
    warnings.simplefilter("ignore")

# try:
import mvFeatureSelector_MV as mvFeatureSelector
import mvFeatureSelector_TM
# except ImportError:
#     print 'importing modulename failed'
#     sys.exit()

def prep(x):
    i = Imputer()
    x = i.fit_transform(np.array(x))
    return StandardScaler().fit_transform(x)

mvReliefF_version = 2

address = r'./data_digits'
    
d1 = pd.read_csv('data_digits/mfeat-fac2.txt',sep=' ', header=None)
d2 = pd.read_csv('data_digits/mfeat-fou',sep=' ', header=None)

d1_columns = d1.columns.values.tolist()
d2_columns = d2.columns.values.tolist()

# there are 10 classes going from 1 to 10 in ascending order
y = [i//200 for i in range(2000)]

spec = cluster.SpectralClustering(n_clusters=10, eigen_solver='arpack', affinity="nearest_neighbors")

w_address = r'./10-fold-digits-results/'

anita = open(os.path.join(w_address, '10_fold_runs.txt'),'w')

anita_results = dict()

for i in range(10):

	d1 = pd.read_csv('data_digits/mfeat-fac2.txt',sep=' ', header=None)
	d2 = pd.read_csv('data_digits/mfeat-fou',sep=' ', header=None)

	d1_columns = d1.columns.values.tolist()
	d2_columns = d2.columns.values.tolist()

	# there are 10 classes going from 1 to 10 in ascending order
	y = [i//200 for i in range(2000)]

	spec = cluster.SpectralClustering(n_clusters=10, eigen_solver='arpack', affinity="nearest_neighbors")


	start_time = datetime.now().time().strftime("%H:%M:%S")
	print('Start Time = {}'.format(start_time))


	mff = mvFeatureSelector.MultilayerFeature([d1,d2], mvReliefF_version, [spec, spec], [d1_columns, d2_columns], metrics.adjusted_rand_score, y, False)
	mff_results = mff.fit()


	clusters, features, aris = mff.selectBestSubset(w_address,i)
	# print(features)
	drop_features1 = set(d1.columns.values.tolist()).difference(set(features[0]))
	drop_features2 = set(d2.columns.values.tolist()).difference(set(features[1]))

	print('Drop {} features from View1 and {} features from View 2'.format(len(drop_features1), len(drop_features2)))


	d1_MV_post = d1.drop(drop_features1, axis=1)
	d2_MV_post = d2.drop(drop_features2, axis=1)

	print('View 1 has shape {} after postprocessing. View 2 has shape {} after postprocessing.'.format(d1_MV_post.shape, d2_MV_post.shape))

	print(features[0])
	print(d1_MV_post.columns.values.tolist())
	print(features[1])
	print(d2_MV_post.columns.values.tolist())


	drop_features1 = set(d1.columns.values.tolist()).difference(set(features[0]))
	drop_features2 = set(d2.columns.values.tolist()).difference(set(features[1]))

	print('Drop {} features from View1 and {} features from View 2'.format(len(drop_features1), len(drop_features2)))


	d1_MV_post = d1.drop(drop_features1, axis=1)
	d2_MV_post = d2.drop(drop_features2, axis=1)

	data = [prep(d1_MV_post), prep(d2_MV_post)]

	for view in range(len(features)):

		best_cluster = clusters[view]
		q = evaluate_clustering(data[view], best_cluster)

		print('View {} has {} features and {} clusters. The quality of clustering is {}.'.format(view, data[view].shape[1], len(set(best_cluster)), round(q, 5)))
	
		ari_v = ari(best_cluster, y)
		nmi_v = nmi(best_cluster, y)
		print('View {} has alignmenet with ground truth of {}'.format(view, ari_v))
		print('View {} has alignmenet with ground truth of {} using NMI.'.format(view, nmi_v))

		if view not in anita_results:
			anita_results[view] = dict()

		if 'quality' not in anita_results[view]:
			anita_results[view]['quality'] = []
		tq = anita_results[view]['quality'] = []
		tq.append(q)
		anita_results[view]['quality'] = tq

		if 'ARI_y' not in anita_results[view]:
			anita_results[view]['ARI_y'] = []
		ta = anita_results[view]['ARI_y'] = []
		ta.append(ari_v)
		anita_results[view]['ARI_y'] = ta

		if 'NMI_y' not in anita_results[view]:
			anita_results[view]['NMI_y'] = []
		tn = anita_results[view]['NMI_y'] = []
		tn.append(nmi_v)
		anita_results[view]['NMI_y'] = tn

		if 'features' not in anita_results[view]:
			anita_results[view]['features'] = []
		tn = anita_results[view]['features'] = []
		tn.append(data[view].shape[1])
		anita_results[view]['features'] = tn

	ari_v = ari(clusters[0], clusters[1])
	nmi_v = nmi(clusters[0], clusters[1])
	print('The alignment between View 1 and View 2 is {}.'.format(ari_v))
	print('The NMI alignment between View 1 and View 2 is {}.'.format(nmi_v))

	if 'Views_ARI' not in anita_results:
		anita_results['Views_ARI'] = []
	va = anita_results['Views_ARI']
	va.append(ari_v)
	anita_results['Views_ARI'] = va

	if 'Views_NMI' not in anita_results:
		anita_results['Views_NMI'] = []
	vn = anita_results['Views_NMI']
	vn.append(nmi_v)
	anita_results['Views_NMI'] = vn

	ens_clusters_AV = np.array(clusters)
	max_cl = max([len(set(clusters[i])) for i in range(len(clusters))])

	consensus_clustering_labels = CE.cluster_ensembles(ens_clusters_AV, verbose = True, N_clusters_max = max_cl)

	ari_c = ari(consensus_clustering_labels, y)
	nmi_c = nmi(consensus_clustering_labels, y)

	print('The alignment between the consensus clustering labels and the ground truth is {}.'.format(ari_c))
	print('The NMI alignment between the consensus clustering labels and the ground truth is {}.'.format(nmi_c))

	if 'CNS_ARI' not in anita_results:
		anita_results['CNS_ARI'] = []
	va = anita_results['CNS_ARI']
	va.append(ari_c)
	anita_results['CNS_ARI'] = va

	if 'CNS_NMI' not in anita_results:
		anita_results['CNS_NMI'] = []
	vn = anita_results['CNS_NMI']
	vn.append(nmi_c)
	anita_results['CNS_NMI'] = vn


	end_time = datetime.now().time().strftime("%H:%M:%S")
	print('End Time = {}'.format(end_time))

	total_time=(datetime.strptime(end_time,'%H:%M:%S') - datetime.strptime(start_time,'%H:%M:%S'))
	print('Processing time for proposed MV approach: {}'.format(total_time))


	print('\n{}\n\n{}\n\n'.format('*'*200,'*'*200))


anita.write('View 0 number of features:\t{}\n'.format(str(anita_results[0]['features'])))
anita.write('View 0 quality:\t{}\n'.format(str(anita_results[0]['quality'])))
anita.write('View 0 alignment with ground truth (ARI):\t{}\n'.format(str(anita_results[0]['ARI_y'])))
anita.write('View 0 alignment with ground truth (NMI):\t{}\n'.format(str(anita_results[0]['NMI_y'])))
anita.write('View 1 number of features:\t{}\n'.format(str(anita_results[1]['features'])))
anita.write('View 1 quality:\t{}\n'.format(str(anita_results[1]['quality'])))
anita.write('View 1 alignment with ground truth (ARI):\t{}\n'.format(str(anita_results[1]['ARI_y'])))
anita.write('View 1 alignment with ground truth (NMI):\t{}\n'.format(str(anita_results[1]['NMI_y'])))
anita.write('View 0 and View 1 alignment (ARI):\t{}\n'.format(str(anita_results['Views_ARI'])))
anita.write('View 0 and View 1 alignment (NMI):\t{}\n'.format(str(anita_results['Views_NMI'])))
anita.write('Consensus alignment with ground truth (ARI):\t{}\n'.format(str(anita_results['CNS_ARI'])))
anita.write('Consensus alignment with ground truth (NMI):\t{}\n'.format(str(anita_results['CNS_NMI'])))

anita.close()
