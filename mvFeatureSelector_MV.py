from __future__ import division
import random
import scipy.io
import os
import sys
import numpy as np
from sklearn.cluster import AgglomerativeClustering

from skfeature.function.similarity_based import lap_score
from skfeature.function.similarity_based import SPEC
from skfeature.function.similarity_based import reliefF
from skfeature.utility import construct_W
import pandas as pd
import itertools
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from sklearn.cluster import AffinityPropagation

from sklearn import metrics
from sklearn.preprocessing import Imputer
from sklearn.datasets.samples_generator import make_blobs
from sklearn.preprocessing import StandardScaler
from skfeature.utility import construct_W
from collections import Counter
from sklearn.base import BaseEstimator
from operator import itemgetter
from sklearn.preprocessing import Imputer
from sklearn import tree
from IPython.display import Image
from sklearn.externals.six import StringIO  
import matplotlib.pyplot as plt
from sklearn.metrics.cluster import normalized_mutual_info_score
import copy
from get_best_clusters_norm import get_best_cluster, evaluate_clustering
import math
import sys
import warnings
import csv

if not sys.warnoptions:
    warnings.simplefilter("ignore")



os.chdir('..')
sys.path.append(os.getcwd())
# try:
import reliefFfixed
import reliefF
import mvreliefFmd
import mvreliefFmh
import mvreliefFmdmh
# except ImportError:
#     print 'importing modulename failed'

os.chdir(os.path.dirname(os.path.abspath(__file__)))

def spec(d,a,b,c):
	kwargs_W = {"style":-1}
	W = construct_W.construct_W(d, **kwargs_W)
	return [x*(-1) for x in SPEC.spec(d, W=W)]


class MultilayerFeature(BaseEstimator):  


    def fscorecluster(self, A, B):
        pairsA = set(itertools.chain.from_iterable([itertools.combinations([y[0] for y in enumerate(A) if y[1]==x], 2) for x in set(A)]))
        pairsB = set(itertools.chain.from_iterable([itertools.combinations([y[0] for y in enumerate(B) if y[1]==x], 2) for x in set(B)]))
 
        a = len(pairsA.intersection(pairsB))
        b = len(pairsA.difference(pairsB))
        c = len(pairsB.difference(pairsA))
 
        return (2*a)/(2*a+b+c)

    def __init__(self, featureLayers, fsIndex, clusterModel, fsNames,scoreFunction, y, preset_clustering=False):
        """
        Called when initializing the classifier
        """
        self.featureLayers = featureLayers
        self.orig = copy.copy(featureLayers)
        self.fsIndex = fsIndex
        self.y = y
        self.fsNames = fsNames

        if fsIndex == -2:
            self.f = spec
        elif fsIndex == -1:
            self.f = reliefFfixed.reliefFfixed
        elif fsIndex == 0:
            self.f = reliefF.reliefF
        elif fsIndex == 1:
            self.f = mvreliefFmd.mvreliefFmd
        elif fsIndex == 2:
            self.f =  mvreliefFmh.mvreliefFmh
        else:
            self.f =  mvreliefFmdmh.mvreliefFmdmh

        self.clusterModel = clusterModel
        self.scoreFunction = scoreFunction
        self.initialRatio = self.featureLayers[0].shape[1] / self.featureLayers[1].shape[1]
        self.preset_clustering = preset_clustering

        self.aris = []
        self.clusters = []
        self.features = []
        self.sil_scores = []

    def add(self, n, cls):
        tmpOrig = copy.copy(self.orig)

        for f in self.featureLayers[0].columns.values:
            tmpOrig[0] = tmpOrig[0].drop(f, 1, errors='ignore')

        for f in self.featureLayers[1].columns.values:
            tmpOrig[1] = tmpOrig[1].drop(f, 1, errors='ignore')

        if (tmpOrig[0].shape[1] >= n):
            names = tmpOrig[0].columns.values
            tmpOrig[0] = self._prep(tmpOrig[0])
            score1 = reliefFy.reliefFy(tmpOrig[0], [], cls[0], [])
            fr = reliefFy.feature_ranking(score1)
            best_names = [names[i] for i in fr][:n]
            new_set = self.orig[0][best_names]

            self.featureLayers[0] = pd.concat([self.featureLayers[0], new_set], axis=1)


        if (tmpOrig[1].shape[1] >= n):
            names = tmpOrig[1].columns.values
            tmpOrig[1] = self._prep(tmpOrig[1])
            score2 = reliefFy.reliefFy(tmpOrig[1], [], cls[1], [])

            fr = reliefFy.feature_ranking(score2)
            best_names = [names[i] for i in fr][:n]
            new_set = self.orig[1][best_names]
            self.featureLayers[1] = pd.concat([self.featureLayers[1], new_set], axis=1)




    def fit(self):

        results=[]
        bestScore = 0
        results2=[]
        results3=[]
        orig = self._prep(self.featureLayers[0])
        cnt = 0

        change = [True]*len(self.featureLayers)

        temp_clusters = [None]*len(self.featureLayers)
        subsets = [None]*len(self.featureLayers)
        temp_sil_scores = [None]*len(self.featureLayers)
        # temp_aris = None
        featureWeighs = dict()

        while True:
            cnt +=1

            if cnt > 1:
                temp_clusters = copy.deepcopy(self.clusters[cnt-2])
                # temp_aris = self.aris[cnt-2]
                # temp_features = self.features[cnt-2]
                temp_sil_scores = copy.deepcopy(self.sil_scores[cnt-2])

            for li in range(len(self.featureLayers)):
                if change[li]:
                    # print('We are filling in the subsets')
                    subset = self._prep(self.featureLayers[li])
                    subsets[li] = subset
                    if self.preset_clustering:
                        best_cluster = self.clusterModel[li].fit_predict(subset)
                        sil_score = evaluate_clustering(subset, best_cluster)
                    else:
                        _, best_cluster, _, sil_score = get_best_cluster(subset)

                    # clusters[li] = copy.deepcopy(best_cluster)
                    temp_clusters[li] = copy.deepcopy(best_cluster)
                    # sil_scores[li] = sil_score
                    temp_sil_scores[li] = sil_score
                    print('{} View {} has silhouete score of {}. The chosen number of clusters is {}.'.format(cnt-1,li, sil_score, len(set(best_cluster))))

            if cnt % 10000 == 0:
                self.add(3, clusters)

#check
            # Update feature weights for each combination of views. featureWeighs[i][j] records the feature weigts for view i compared to view j.

            for li in range(len(self.featureLayers)):
                for lj in range(len(self.featureLayers)):
                    if li != lj:
                        if change[li] or change[lj]:
                            if li not in featureWeighs:
                                featureWeighs[li] = dict()
                            if lj not in featureWeighs[li]:
                                featureWeighs[li][lj] = self.f(subsets[li], subsets[lj], temp_clusters[li], temp_clusters[lj])
                            if lj not in featureWeighs:
                                featureWeighs[lj] = dict()
                            if li not in featureWeighs[li]:
                                featureWeighs[lj][li] = self.f(subsets[lj], subsets[li], temp_clusters[lj], temp_clusters[li])

            
            res1 = dict()
            tempBestWorst = math.inf
            tempBestWorst_inds = []
            for li in range(len(self.featureLayers)-1):
                for lj in range(li+1, len(self.featureLayers)):
                    if li not in res1:
                        res1[li] = dict()
                    if lj not in res1[li]:
                        r1 = self.scoreFunction(temp_clusters[li], temp_clusters[lj])
                        res1[li][lj] = r1
                    if r1 <= tempBestWorst:
                        tempBestWorst = r1
                        tempBestWorst_inds = [li, lj]


            print(res1)	
            feats = [self.featureLayers[li].columns.values.tolist() for li in range(len(self.featureLayers))]
            results.append(temp_clusters)
            results2.append(feats)
            results3.append([res1])

            self.aris.append(res1)
            self.clusters.append(temp_clusters)
            self.features.append(feats)
            self.sil_scores.append(temp_sil_scores)


            if tempBestWorst > bestScore:
                bestScore = tempBestWorst
            
            print("Currently worst score: ", tempBestWorst, ", Len L1: ", self.featureLayers[tempBestWorst_inds[0]].shape[1], ", Len L2: ", self.featureLayers[tempBestWorst_inds[1]].shape[1], ", Combination of view ", tempBestWorst_inds[0], " and view ", tempBestWorst_inds[1])

            subsets_shape = [subsets[li].shape[1] for li in range(len(self.featureLayers))]

            if sum(subsets_shape) == len(self.featureLayers):
                break

            changed_view = self._removeWorst(featureWeighs, temp_sil_scores)
            change = [False]*len(self.featureLayers)
            change[changed_view] = True


        # with open("./temp_results/out_clusters.csv","w") as f:
        #     wr = csv.writer(f)
        #     wr.writerows(self.clusters)
        print('Best Score: ', bestScore)
        return [results, results2, results3]

    def _prep(self, x):
        i = Imputer()
        x = i.fit_transform(np.array(x))
        return StandardScaler().fit_transform(x)

    def prep(self, x):
        i = Imputer()
        x = i.fit_transform(np.array(x))
        return StandardScaler().fit_transform(x)

    def get_worst_feature(self, worst_features, tie):
        view = None
        feature = None
        value = math.inf

        for t in tie:
            if worst_features[t][1] < value:
                value = worst_features[t][1]
                view = t
                feature = worst_features[t][0]

        return view, feature


    def select_worst_feature(self, li, featureWeighs, worst_feature, sil_scores):
        max_count = max(worst_feature)
        # print('Max count {}'.format(max_count))

        worst_weights = [math.inf for wf in worst_feature]
        # print('Worst weights {}'.format(worst_weights))
        for wf in range(len(worst_feature)):
            if worst_feature[wf] == max_count:
                # print('We entered the loop.')
                smf = [featureWeighs[li][lj][wf]/featureWeighs[li][lj][np.argmax(featureWeighs[li][lj])] for lj in featureWeighs[li]]
                # smf = [featureWeighs[li][lj][wf]/featureWeighs[li][lj][np.argmax(featureWeighs[li][lj])] for lj in comp_views[li][wf]]
                sv = sum(smf)/len(smf)+sil_scores[li]
                worst_weights[wf] = sv

        wf = np.argmin(worst_weights)
        return wf, worst_weights[wf]

    def get_worst_value(self, li, worst_feature,featureWeighs, sil_scores):
        index = np.argmax(worst_feature)

        smf = [featureWeighs[li][lj][index]/featureWeighs[li][lj][np.argmax(featureWeighs[li][lj])] for lj in featureWeighs[li]]
        # smf = [featureWeighs[li][lj][index]/featureWeighs[li][lj][np.argmax(featureWeighs[li][lj])] for lj in comp_views[li][index]]
        wv = sum(smf)/len(smf)+sil_scores[li]

        return wv


    def _removeWorst(self, featureWeighs, sil_scores):
        worst_features = dict()

        for li in featureWeighs:
            # print('Feature weights {}'.format(self.featureLayers[li].shape[1]))
            worst_feature = [0 for f in range(self.featureLayers[li].shape[1])]
            for lj in featureWeighs[li]:
                # print(worst_feature)
                wfi = np.argmin(featureWeighs[li][lj])
                # print('WFI {}'.format(wfi))
                worst_feature[wfi] = worst_feature[wfi]+1
            
            if worst_feature.count(max(worst_feature)) > 1:
                wf, wvalue = self.select_worst_feature(li, featureWeighs, worst_feature, sil_scores)
                worst_features[li] = [wf, wvalue, worst_feature[wf]]
            else:
                wvalue = self.get_worst_value(li, worst_feature,featureWeighs, sil_scores)
                worst_features[li] = [np.argmax(worst_feature),wvalue,worst_feature[np.argmax(worst_feature)]]


        considered_views = []
        for li in range(len(self.featureLayers)):
            if self.featureLayers[li].shape[1] > 1:
                considered_views.append(li)


        # print(considered_views)
        wv, wf = self.get_worst_feature(worst_features, considered_views)
        
        for t in considered_views:
            print('View {} worst feature is {}. Its been the worst in {} combinations.'.format(t, self.fsNames[t][worst_features[t][0]], worst_features[t][2]))

        # print('Removing feature {} from view {}'.format(worst_features[wv][0], wv))
        print('\nRemoving feature {} from view {}\n{}'.format(self.fsNames[wv][wf], wv, '='*50))
        self.featureLayers[wv] = self.featureLayers[wv].drop(self.featureLayers[wv].columns.values[wf], 1)
        del self.fsNames[wv][wf]


    def fsFunction(self, d, c):
        if self.fsIndex == 0:
            kwargs_W = {"metric":"euclidean","neighbor_mode":"knn","weight_mode":"heat_kernel","k":5,'t':1}
            W = construct_W.construct_W(d, **kwargs_W)
            return lap_score.lap_score(d, W=W)
        if self.fsIndex == 1:
            kwargs_W = {"style":0}
            W = construct_W.construct_W(d, **kwargs_W)
            return SPEC.spec(d, W=W)
        if self.fsIndex == 2:
            kwargs_W = {}
            W = construct_W.construct_W(d, **kwargs_W)
            return reversed(reliefF.reliefF(d, c, W=W))

    def selectBestSubset(self,folder='./temp_results',i=''):
        quality = []

        temp_clusters = open(os.path.join(folder,'clusters{}.txt'.format(i)),'w')
        temp_aris = open(os.path.join(folder,'aris{}.txt'.format(i)),'w')
        temp_sil_scores = open(os.path.join(folder,'sil_scores{}.txt'.format(i)),'w')
        temp_features = open(os.path.join(folder,'features{}.txt'.format(i)),'w')



        for i in range(len(self.aris)):
            temp_quality = []
            for li in self.aris[i]:
                for lj in self.aris[i][li]:
                    # value = (self.sil_scores[i][li]+self.sil_scores[i][lj])*self.aris[i][li][lj]
                    value = self.aris[i][li][lj]
                    temp_quality.append(value)
            quality.append(sum(temp_quality))

            temp_clusters.write('{}\n'.format(self.clusters[i]))
            temp_aris.write('{}\n'.format(self.aris[i]))
            temp_features.write('{}\n'.format(self.features[i]))
            temp_sil_scores.write('{}\n'.format(self.sil_scores[i]))
       

        temp_clusters.close()
        temp_aris.close()
        temp_sil_scores.close()
        temp_features.close()


        tq = -math.inf
        combos = []

        for i in range(len(quality)):
            if quality[i] > tq:
                combos = [i]
                tq = quality[i]

            elif abs(quality[i] - tq) <= 1e-10:
                combos.append(i)


        index = max(combos)

        # print(self.clusters[index])

        print('Best index: {}'.format(index))
        print('Cluster combinations: {}'.format(len(self.clusters)))
        print('ARI combinations: {}'.format(len(self.aris)))
        print('Feature combinations: {}'.format(len(self.features)))
        print('Sil scores combinations: {}'.format(len(self.sil_scores)))


        return self.clusters[index], self.features[index], self.aris[index]
