# 
# 
# Please note, we do not have permission to share ADNI data. Data is available on: http://adni.loni.usc.edu/
# 
# 
from __future__ import division
from sklearn.cluster import MeanShift, estimate_bandwidth
from collections import Counter
from sklearn.preprocessing import Imputer
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn import metrics
import sys
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import spectral_clustering
from sklearn.cluster import AffinityPropagation
from sklearn import cluster
import os
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Imputer
from get_best_clusters_norm import get_best_cluster, evaluate_clustering
from sklearn.metrics import adjusted_rand_score as ari
import Cluster_Ensembles as CE
from sklearn.metrics import normalized_mutual_info_score as nmi
from sklearn.metrics import silhouette_score as sc
import copy

import sys
import warnings

from datetime import datetime

if not sys.warnoptions:
    warnings.simplefilter("ignore")


def prep(x):
    i = Imputer()
    x = i.fit_transform(np.array(x))
    return StandardScaler().fit_transform(x)

mvReliefF_version = 2

NMIs = []
sscores = []
sscores_DX = []

clusters_archive = []

start_time = datetime.now().time().strftime("%H:%M:%S")
print('Start Time = {}'.format(start_time))

for i in range(10):
    
    d1 = pd.read_csv('./ADNI_data/m84/clinical.csv')
    d2 = pd.read_csv('./ADNI_data/m84/bio.csv')


    # get the diagnosis of patients on their last visit
    y = d2['DX'].values.tolist()
    y1 = d2['DX_bl'].values.tolist()

    y3 = ['{}-to-{}'.format(y1[i], y[i]) for i in range(len(y))]

    df_merged = pd.merge(d1,d2,on=['RID'])

    DX = d2['DX'].values.tolist()

    df_merged.drop(['RID','DX', 'DX_bl'],axis=1, inplace=True)

    d1.drop(['RID'],axis=1, inplace=True)
    d2.drop(['RID','DX', 'DX_bl'],axis=1, inplace=True)
    cluster = get_best_cluster(prep(df_merged))
    print(len(cluster[1]))
    print(len(y))

    data = [prep(d1), prep(d2)]
    clusters_archive.append(cluster[1])

    # clusters = []
    for view in range(len(data)):
        best_cluster = cluster[1]
        print('View {} has alignmenet with ground truth of {}'.format(view, ari(best_cluster, y)))
        print('View {} has alignmenet with ground truth of {} using NMI.'.format(view, nmi(best_cluster, y)))

    NMIs.append(nmi(best_cluster, y))
    clusters_archive.append(cluster[1])
    sscores.append(evaluate_clustering(prep(df_merged), cluster[1]))
    sscores_DX.append(evaluate_clustering(prep(df_merged), DX))

    ccs = ['Cluster {}'.format(c) for c in cluster[1]]
    df_cluster = copy.deepcopy(df_merged)
    df_cluster['cluster']=ccs

    df_DX = copy.deepcopy(df_merged)
    df_DX['DX']=DX

    df_cluster.to_csv('./ADNI_results/singleview/cluster/combined_views_{}.csv'.format(i), index=False)
    df_DX.to_csv('../ADNI_results/singleview/DX/combined_views.csv_{}'.format(i), index=False)


end_time = datetime.now().time().strftime("%H:%M:%S")
print('End Time = {}'.format(end_time))

total_time=(datetime.strptime(end_time,'%H:%M:%S') - datetime.strptime(start_time,'%H:%M:%S'))
print('Processing time for proposed MV approach: {}'.format(total_time))


print('\n{}\n\n{}\n\n'.format('*'*200,'*'*200))
print(NMIs)
print(sscores)
print(sscores_DX)
index = np.argmax(NMIs)
index = np.argmax(sscores)
print(index)


print('NMI: {} +- {}'.format(np.mean(NMIs), np.std(NMIs)))
print('SS: {} +- {}'.format(np.mean(sscores), np.std(sscores)))
print('SSDX: {} +- {}'.format(np.mean(sscores_DX), np.std(sscores_DX)))

print('NMI: {}'.format(NMIs[index]))
print('SS: {}'.format(sscores[index]))
print('SSDX: {}'.format(sscores_DX[index]))

