import sys
import warnings
from copy import deepcopy

from datetime import datetime

if not sys.warnoptions:
    warnings.simplefilter("ignore")

def prep(x):
    i = Imputer()
    x = i.fit_transform(np.array(x))
    return StandardScaler().fit_transform(x)

def get_norm_silhouette_SM(indices, sil_score, sil_samples):
	ratios = []
	for cls in indices:
		inds = indices[cls]
		t_inds = []
		for ti in inds:
			if sil_samples[ti]-sil_score >= 1e-10:
				t_inds.append(ti)
		ratio = (1.0*len(t_inds)/len(inds))
		ratios.append(ratio)

	return((sum(ratios)/len(ratios))*sil_score)


def get_norm_silhouette_SN(indices, sil_score, sil_samples):
	ratios = []
	for cls in indices:
		inds = indices[cls]
		t_inds = []
		for ti in inds:
			if sil_samples[ti]-sil_score >= 1e-10:
				t_inds.append(ti)
		ratio = (1.0*len(t_inds)/len(inds))*(1.0*len(inds)/len(sil_samples))
		ratios.append(ratio)

	return(sum(ratios)*sil_score)

def get_cluster_indices(cls):
	indices = dict()
	for i in range(len(cls)):
		if cls[i] not in indices:
			indices[cls[i]]=[]
		l = indices[cls[i]]
		l.append(i)
		indices[cls[i]]=l
	return indices