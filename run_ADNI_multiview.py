# 
# 
# Please note, we do not have permission to share ADNI data. Data is available on: http://adni.loni.usc.edu/
# 
# 

from __future__ import division
from sklearn.cluster import MeanShift, estimate_bandwidth
from collections import Counter
from sklearn.preprocessing import Imputer
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn import metrics
import sys
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import spectral_clustering
from sklearn.cluster import AffinityPropagation
from sklearn import cluster
import os
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Imputer
from get_best_clusters_norm import get_best_cluster, evaluate_clustering
# from get_best_clusters_norm2 import get_optimal_clusters as get_best_cluster
# from get_best_clusters_norm2 import evaluate_clustering
from sklearn.metrics import adjusted_rand_score as ari
import Cluster_Ensembles as CE
from sklearn.metrics import normalized_mutual_info_score as nmi
from sklearn.metrics import silhouette_score as sc
import copy

import sys
import warnings

from datetime import datetime

if not sys.warnoptions:
    warnings.simplefilter("ignore")

# try:
import mvFeatureSelector_MV as mvFeatureSelector
import mvFeatureSelector_TM
# except ImportError:
#     print 'importing modulename failed'
#     sys.exit()

def prep(x):
    i = Imputer()
    x = i.fit_transform(np.array(x))
    return StandardScaler().fit_transform(x)

mvReliefF_version = 2

keep_columns_all_iterations = []
clusters_archive_all_iterations = []
sscores_all_iterations = []
sscores_dx_all_iterations = []
combo = []
NMIs_all_iterations = []

address = r'./ADNI_results/multiview/MVReliefF/results'

for c in [2,3,4,5,6]:

	NMIs = []

	keep_columns = []
	clusters_archive = []
	sscores = []
	sscores_dx = []

	start_time = datetime.now().time().strftime("%H:%M:%S")
	print('Start Time = {}'.format(start_time))

	for i in range(10):
	    
	    d1 = pd.read_csv('./ADNI_data/m84/clinical.csv')
	    d2 = pd.read_csv('./ADNI_data/m84/bio.csv')


		DX = d2['DX'].values.tolist()

		d1_columns = d1.columns.values.tolist()
		d2_columns = d2.columns.values.tolist()

		# get the diagnosis of patients on their last visit
		y = d2['DX'].values.tolist()

		spec = cluster.SpectralClustering(n_clusters=3, eigen_solver='arpack', affinity="nearest_neighbors")

		df_merged = pd.merge(d1,d2,on=['RID'])

		d1.drop(['RID'],axis = 1, inplace=True)
		d2.drop(['RID','DX', 'DX_bl'],axis=1, inplace=True)


		print(df_merged.shape)

		d1_columns = d1.columns.values.tolist()
		d2_columns = d2.columns.values.tolist()


		print('D1: {}'.format(d1.shape))
		print('D2: {}'.format(d2.shape))


		mff = mvFeatureSelector.MultilayerFeature([d1,d2], mvReliefF_version, [spec, spec], [d1_columns, d2_columns], metrics.adjusted_rand_score, y, True)
		mff_results = mff.fit()

		clusters, features, aris = mff.selectBestSubset(address)
		# print(features)
		drop_features1 = set(d1.columns.values.tolist()).difference(set(features[0]))
		drop_features2 = set(d2.columns.values.tolist()).difference(set(features[1]))

		print('Drop {} features from View1 and {} features from View 2'.format(len(drop_features1), len(drop_features2)))


		d1_MV_post = d1.drop(drop_features1, axis=1)
		d2_MV_post = d2.drop(drop_features2, axis=1)

		print('View 1 has shape {} after postprocessing. View 2 has shape {} after postprocessing.'.format(d1_MV_post.shape, d2_MV_post.shape))

		print(features[0])
		print(d1_MV_post.columns.values.tolist())
		print(features[1])
		print(d2_MV_post.columns.values.tolist())


		drop_features1 = set(d1.columns.values.tolist()).difference(set(features[0]))
		drop_features2 = set(d2.columns.values.tolist()).difference(set(features[1]))

		print('Drop {} features from View1 and {} features from View 2'.format(len(drop_features1), len(drop_features2)))

		keep_columns.append([features[0], features[1]])

		keep_features = features[0]
		keep_features.extend(features[1])


		d1_MV_post = d1.drop(drop_features1, axis=1)
		d2_MV_post = d2.drop(drop_features2, axis=1)

		data = [prep(d1_MV_post), prep(d2_MV_post)]

		print(d1_MV_post.shape)
		print(d2_MV_post.shape)

		# clusters = []
		for view in range(len(data)):
			best_cluster = clusters[view]
			print('View {} has {} features and {} clusters. The quality of clustering is {}.'.format(view, data[view].shape[1], len(set(best_cluster)), round(evaluate_clustering(data[view], best_cluster), 5)))
			print('View {} has alignmenet with ground truth of {}'.format(view, ari(best_cluster, y)))
			print('View {} has alignmenet with ground truth of {} using NMI.'.format(view, nmi(best_cluster, y)))


		print('The alignment between View 1 and View 2 is {}.'.format(ari(clusters[0], clusters[1])))
		print('The NMI alignment between View 1 and View 2 is {}.'.format(nmi(clusters[0], clusters[1])))

		print(clusters[0].shape)
		print(clusters[1].shape)

		ens_clusters_AV = np.array(clusters)
		max_cl = max([len(set(clusters[i])) for i in range(len(clusters))])

		consensus_clustering_labels = CE.cluster_ensembles(ens_clusters_AV, verbose = True, N_clusters_max = max_cl)

		print('The alignment between the consensus clustering labels and the ground truth is {}.'.format(ari(consensus_clustering_labels, y)))
		print('The NMI alignment between the consensus clustering labels and the ground truth is {}.'.format(nmi(consensus_clustering_labels, y)))

		NMIs.append(nmi(consensus_clustering_labels, y))
		clusters_archive.append(consensus_clustering_labels)
		df_best = df_merged[keep_features]

		print(df_best.shape)
		print(consensus_clustering_labels.shape)

		

		sscores.append(evaluate_clustering(prep(df_best), consensus_clustering_labels))
		sscores_dx.append(evaluate_clustering(prep(df_best), DX))

		cclabels = ['Cluster {}'.format(c) for c in consensus_clustering_labels]

		df_consensus = copy.deepcopy(df_best)
		df_consensus['Cluster'] = cclabels

		df_DX = copy.deepcopy(df_best)
		df_DX['DX'] = DX


		df_consensus.to_csv('./ADNI_results/multiview/MVReliefF/cluster/NMI_JOURNAL_{}_iter{}.csv'.format(c,i), index=False)
		df_DX.to_csv('./ADNI_results/multiview/MVReliefF/DX/DX_NMI_JOURNAL_{}_iter{}.csv'.format(c,i), index=False)

	end_time = datetime.now().time().strftime("%H:%M:%S")
	print('End Time = {}'.format(end_time))

	total_time=(datetime.strptime(end_time,'%H:%M:%S') - datetime.strptime(start_time,'%H:%M:%S'))
	print('Processing time for proposed MV approach: {}'.format(total_time))


	print('\n{}\n\n{}\n\n'.format('*'*200,'*'*200))

	
	print('Number of clusters: {}'.format(c))



	index = np.argmax(sscores)
	print(keep_columns[index])
	print(clusters_archive[index])
	print(index)

	print('NMI: {} +- {}'.format(np.mean(NMIs), np.std(NMIs)))
	print('SS: {} +- {}'.format(np.mean(sscores), np.std(sscores)))
	print('SSDX: {} +- {}'.format(np.mean(sscores_dx), np.std(sscores_dx)))

	combo.append([c,index])

	keep_columns_all_iterations.append(keep_columns[index])
	clusters_archive_all_iterations.append(clusters_archive[index])
	sscores_all_iterations.append(sscores[index])
	sscores_dx_all_iterations.append(sscores_dx[index])
	NMIs_all_iterations.append(NMIs[index])


index = np.argmax(sscores_all_iterations)


print(keep_columns_all_iterations[index])
print(clusters_archive_all_iterations[index])
print('COMBO: {}'.format(combo[index]))
print('Best NMI: {}'.format(NMIs_all_iterations[index]))
print('Best SC: {}'.format(sscores_all_iterations[index]))
print('Best SC_DX: {}'.format(sscores_dx_all_iterations[index]))