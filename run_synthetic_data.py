from __future__ import division
import os
from sklearn.cluster import MeanShift, estimate_bandwidth
from sklearn.cluster import AffinityPropagation
from sklearn import cluster, datasets
from sklearn.neighbors import kneighbors_graph
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans, MiniBatchKMeans
from sklearn.preprocessing import Imputer
from sklearn import metrics
from sklearn import cluster
import sys
from sklearn.preprocessing import StandardScaler
import random
from collections import Counter
from get_best_clusters import get_best_cluster
import warnings
import csv
import Cluster_Ensembles as CE
from sklearn.metrics import adjusted_rand_score as ari
from sklearn.metrics import normalized_mutual_info_score as nmi


import sys
import warnings

from datetime import datetime

if not sys.warnoptions:
    warnings.simplefilter("ignore")



# try:
import mvFeatureSelector_MV as mvFeatureSelector
# except ImportError:
#     print 'importing modulename failed'

def prep(x):
    i = Imputer()
    x = i.fit_transform(np.array(x))
    return StandardScaler().fit_transform(x)


mvResults = [[],[],[],[],[]]
rResults = [[],[],[],[],[]]

for i in range(10):

	start_time = datetime.now().time().strftime("%H:%M:%S")
	print('Start Time = {}'.format(start_time))

	address = r'./synthetic_data'

	d1_address = os.path.join(address, 'view1.csv')
	d2_address = os.path.join(address, 'view2.csv')
	d3_address = os.path.join(address, 'view3.csv')
	d4_address = os.path.join(address, 'view4.csv')
	d5_address = os.path.join(address, 'view5.csv')

	d1 = pd.read_csv(d1_address)
	d2 = pd.read_csv(d2_address)
	d3 = pd.read_csv(d3_address)
	d4 = pd.read_csv(d4_address)
	d5 = pd.read_csv(d5_address)

	d1_address_labels = os.path.join(address, 'view1_labels.csv')
	d2_address_labels = os.path.join(address, 'view2_labels.csv')
	d3_address_labels = os.path.join(address, 'view3_labels.csv')
	d4_address_labels = os.path.join(address, 'view4_labels.csv')
	d5_address_labels = os.path.join(address, 'view5_labels.csv')

	d1_labels = pd.read_csv(d1_address_labels).to_numpy().flatten()
	d2_labels = pd.read_csv(d2_address_labels).to_numpy().flatten()
	d3_labels = pd.read_csv(d3_address_labels).to_numpy().flatten()
	d4_labels = pd.read_csv(d4_address_labels).to_numpy().flatten()
	d5_labels = pd.read_csv(d5_address_labels).to_numpy().flatten()

	labels = [d1_labels,d2_labels,d3_labels,d4_labels,d5_labels]

	d1_columns = d1.columns.values.tolist()
	d2_columns = d2.columns.values.tolist()
	d3_columns = d3.columns.values.tolist()
	d4_columns = d4.columns.values.tolist()
	d5_columns = d5.columns.values.tolist()

	#get clustering algorithm
	spec2 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)
	spec1 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)
	spec3 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)
	spec4 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)
	spec5 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)



	#we know the desired clusters in advance
	y = [random.randint(1,4) for i in range(d1.shape[0])]


	mvReliefF_version = 2

	#get result
	mff = mvFeatureSelector.MultilayerFeature([d1,d2,d3,d4, d5], mvReliefF_version, [spec1, spec2, spec3, spec4, spec5], [d1_columns, d2_columns, d3_columns, d4_columns, d5_columns], metrics.normalized_mutual_info_score, y)
	mff_results = mff.fit()

	clusters, features, aris = mff.selectBestSubset()
	print(features)

	ens_clusters_AV = np.array(clusters)
	max_cl = max([len(set(clusters[i])) for i in range(len(clusters))])

	consensus_clustering_labels = CE.cluster_ensembles(ens_clusters_AV, verbose = True, N_clusters_max = max_cl)

	for view in range(len(features)):
		print('View {} has {} features and {} clusters.'.format(view, len(features[view]), len(set(clusters[view]))))
		alc = nmi(consensus_clustering_labels, labels[view])
		print('Alignment of view {} labels with the consensus labels is {}'.format(view, alc))
		mvResults[view].append(alc)



	end_time = datetime.now().time().strftime("%H:%M:%S")
	print('End Time = {}'.format(end_time))

	total_time=(datetime.strptime(end_time,'%H:%M:%S') - datetime.strptime(start_time,'%H:%M:%S'))
	print('Processing time: {}'.format(total_time))

	print('\n\nRESULTS USING RELIEFF\n\n')


for i in range(10):
	mvReliefF_version = 0

	address = r'./synthetic_data'

	d1_address = os.path.join(address, 'view1.csv')
	d2_address = os.path.join(address, 'view2.csv')
	d3_address = os.path.join(address, 'view3.csv')
	d4_address = os.path.join(address, 'view4.csv')
	d5_address = os.path.join(address, 'view5.csv')

	d1 = pd.read_csv(d1_address)
	d2 = pd.read_csv(d2_address)
	d3 = pd.read_csv(d3_address)
	d4 = pd.read_csv(d4_address)
	d5 = pd.read_csv(d5_address)

	d1_address_labels = os.path.join(address, 'view1_labels.csv')
	d2_address_labels = os.path.join(address, 'view2_labels.csv')
	d3_address_labels = os.path.join(address, 'view3_labels.csv')
	d4_address_labels = os.path.join(address, 'view4_labels.csv')
	d5_address_labels = os.path.join(address, 'view5_labels.csv')

	d1_labels = pd.read_csv(d1_address_labels).to_numpy().flatten()
	d2_labels = pd.read_csv(d2_address_labels).to_numpy().flatten()
	d3_labels = pd.read_csv(d3_address_labels).to_numpy().flatten()
	d4_labels = pd.read_csv(d4_address_labels).to_numpy().flatten()
	d5_labels = pd.read_csv(d5_address_labels).to_numpy().flatten()

	labels = [d1_labels,d2_labels,d3_labels,d4_labels,d5_labels]

	d1_columns = d1.columns.values.tolist()
	d2_columns = d2.columns.values.tolist()
	d3_columns = d3.columns.values.tolist()
	d4_columns = d4.columns.values.tolist()
	d5_columns = d5.columns.values.tolist()

	#get clustering algorithm
	spec2 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)
	spec1 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)
	spec3 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)
	spec4 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)
	spec5 = cluster.SpectralClustering(n_clusters=4, eigen_solver='arpack', affinity="nearest_neighbors", n_neighbors=30)



	#we know the desired clusters in advance
	y = [random.randint(1,4) for i in range(d1.shape[0])]


	#get result
	mff = mvFeatureSelector.MultilayerFeature([d1,d2,d3,d4,d5], mvReliefF_version, [spec1, spec2, spec3, spec4, spec5], [d1_columns, d2_columns, d3_columns, d4_columns, d5_columns], metrics.normalized_mutual_info_score, y)
	mff_results = mff.fit()


	clusters, features, aris = mff.selectBestSubset()
	print(features)

	ens_clusters_AV = np.array(clusters)
	max_cl = max([len(set(clusters[i])) for i in range(len(clusters))])

	consensus_clustering_labels = CE.cluster_ensembles(ens_clusters_AV, verbose = True, N_clusters_max = max_cl)

	for view in range(len(features)):
		print('R: View {} has {} features and {} clusters.'.format(view, len(features[view]), len(set(clusters[view]))))
		alcr = nmi(consensus_clustering_labels, labels[view])
		print('R: Alignment of view {} labels with the consensus labels is {}'.format(view, alcr))
		rResults[view].append(alcr)


print('\n\nMVRESULT\n\n')
for view in range(len(labels)):
	print(mvResults[view])

print('\n\nRRRRESULT\n\n')
for view in range(len(labels)):
	print(rResults[view])


print('\n\nTABLE\n\n')
print('View\t\\mvReliefF\treliefF')
for view in range(len(labels)):
	print('View {}\t{}({})\t{}({})'.format(view, round(np.mean(mvResults[view])), round(np.std(mvResults[view])), round(np.mean(rResults[view])), round(np.std(rResults[view]))))


# address = r'./synthetic_data'

# d1_address_labels = os.path.join(address, 'view1_labels.csv')
# d2_address_labels = os.path.join(address, 'view2_labels.csv')
# d3_address_labels = os.path.join(address, 'view3_labels.csv')
# d4_address_labels = os.path.join(address, 'view4_labels.csv')
# d5_address_labels = os.path.join(address, 'view5_labels.csv')

# d1_labels = pd.read_csv(d1_address_labels).to_numpy().flatten()
# d2_labels = pd.read_csv(d2_address_labels).to_numpy().flatten()
# d3_labels = pd.read_csv(d3_address_labels).to_numpy().flatten()
# d4_labels = pd.read_csv(d4_address_labels).to_numpy().flatten()
# d5_labels = pd.read_csv(d5_address_labels).to_numpy().flatten()

# temp_labels = [d1_labels, d2_labels, d3_labels, d4_labels, d5_labels]

# for tl in temp_labels:
# 	print(round(nmi(d1_labels,tl), 3))



